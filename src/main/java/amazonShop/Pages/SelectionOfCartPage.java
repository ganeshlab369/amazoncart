package amazonShop.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.qameta.allure.Step;

public class SelectionOfCartPage extends BasePage {

	public SelectionOfCartPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//*********Web Elements*********
	By TodaysDealnavBtn = By.cssSelector("#nav-xshop > a:nth-child(4)");
	
	
	//*********Page Methods*********
	@Step ("Navigate to Today's Deal Page")
	public SelectionOfCartPage Navigate_to_Todays_Deal_Page () {
	
		click(TodaysDealnavBtn);
		
		return this;
	}
	
	
	
}
