package amazonShop.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import io.qameta.allure.Step;
public class LogoutPage extends BasePage {

	
public LogoutPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//*********Web Elements*********
	//Logout
	By logoutBtn = By.cssSelector("#nav-item-signout > span:nth-child(1)");
	
	//*********Page Methods*********

    @Step ("Logout Account")
    public LogoutPage logoutBtn (){
        
    	//Click Sign out
        click(logoutBtn);
        

        return this;
    }
}
