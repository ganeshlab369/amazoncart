package amazonShop.Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
public class LoginPage extends BasePage {

	public LoginPage (WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	//*********Web Elements*********
	//Login Page
    By emailBy = By.id("ap_email");
    By passwordBy = By.id("ap_password");
    By loginBtn = By.id("signInSubmit");
	
    //*********Page Methods*********

    @Step ("Fill email: {0}, method: {method} step... ")
    public LoginPage enterEmail (String email){
        
    	//Enter email
        writeText(emailBy,email);
        

        return this;
    }
    @Step ("Fill password: {0}, method: {method} step... ")
    public LoginPage enterPassword (String pwd){
        
    	//Enter email
        writeText(passwordBy,pwd);
        

        return this;
    }
    @Step ("Click Login Button: {0}, method: {method} step...")
    public LoginPage loginBtn () {
    	
    	click(loginBtn);
    	
    	return this;
    }

}
