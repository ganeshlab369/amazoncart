package amazonShop.tests;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.*;
import org.testng.IHookCallBack;
import org.testng.ITestResult;
import org.testng.annotations.*;
import io.qameta.allure.Attachment;

public class BaseTest {
	
	public static WebDriver driver;
	
	@BeforeClass
	public void Open_Browser () {
        //Create a Firefox driver. All test classes use this.
        driver = new FirefoxDriver();

        //Maximize Window
        driver.manage().window().maximize();
  
    }
	/**
	 * screenShot method is invoked whenever the Testcase is Failed.
	 * @param name
	 * @param driver
	 * @return
	 */
	@Attachment(value = "Screenshot of {0}", type = "image/png")
	public byte[] saveScreenshot(String name, WebDriver driver) {
		return (byte[]) ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
	}

	public void run(IHookCallBack iHookCallBack, ITestResult iTestResult) {
		iHookCallBack.runTestMethod(iTestResult);
		if (iTestResult.getThrowable() != null) {
			this.saveScreenshot(iTestResult.getName(), driver);
		}
	}
	@AfterClass
	public void Close_Browser () {
		driver.quit();

	}
}
